# -*- coding: utf-8 -*-

from cmdutils import cmdhelper as ch

import unittest


class AdvancedTestSuite(unittest.TestCase):
    """Advanced test cases."""

    def test_exe_remote_cmd(self):
        stdout, stderr = ch.exe_remote_cmd('root', 'vultr-agent', 'echo "Hello"')
        print(stdout)
        self.assertEqual('Hello', stdout)

    def test_exe_local_simple_cmd(self):
        stdout, stderr = ch.exe_local_simple_cmd('echo Hello')
        pout = stdout.decode('utf-8').strip()
        print(pout)
        self.assertEqual('Hello', pout)

    def test_exe_local_cmd_pipe(self):
        stdout, stderr = ch.exe_local_cmd_pipe('echo Hello', 'grep e', 'grep o')
        pout = stdout.decode('utf-8').strip()
        print(pout)
        self.assertEqual('Hello', pout)


if __name__ == '__main__':
    unittest.main()
