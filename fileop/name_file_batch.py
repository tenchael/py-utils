import os
import shutil


def batch_name_files(dir_s):
    the_files = os.listdir(path=dir_s)
    count = 1
    for filename in the_files:
        if not os.path.isfile(dir_s + "/" + filename):
            continue
        suffix = ""
        index = filename.rfind(".")
        if index == -1:
            suffix = ""
        else:
            suffix = filename[index:]

        new_name = "{:0>5}".format(count) + suffix
        shutil.move(dir_s + "/" + filename, dir_s + "/" + new_name)
        print(filename, "-->", new_name)
        count += 1


dir_s = input("path:")
print(dir_s)
the_list = os.listdir(dir_s)
print(the_list)
batch_name_files(dir_s)
